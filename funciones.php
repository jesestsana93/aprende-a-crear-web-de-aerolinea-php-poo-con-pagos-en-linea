<?php
function fechaEntexto($fecha){
    date_default_timezone_set('America/Mexico_city');
    //date_default_timezone_set('America/Argentina/Buenos_Aires');
    //date_default_timezone_set('America/Bogota');
    $numeroDia=date('d',strtotime($fecha));
    $dia=date('l',strtotime($fecha));
    $mes=date('F',strtotime($fecha));
    $anio=date('Y',strtotime($fecha));
    $dia_ES=array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
    $dia_EN=array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
    
    $nombredia=str_replace($dia_EN,$dia_ES,$dia);

    $meses_ES=array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
    $meses_EN=array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
    
    $nombremes=str_replace($meses_EN,$meses_ES,$mes);

    return $nombredia." ".$numeroDia." de ".$nombremes." de ".$anio;
}