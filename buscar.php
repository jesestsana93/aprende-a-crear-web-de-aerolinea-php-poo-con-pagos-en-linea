<!DOCTYPE html>
<html lang="es" itemscope itemtype="http://schema.org/WebPage">
<head>
  <title>AeroTour</title>

  <meta charset="utf-8">
<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="expires" content="0">

<!-- CSRF Token -->
<meta name="csrf-token" content="Q3MAREqJb3eFRWWrov32D7fPVrVaTnQ8lpsXHsuo">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Cache-control" content="no-cache" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="expires" content="0" />

<meta name="author" content="Aviatoor">
<meta name="description" content="Pasajes de Bus Aviatoor, Pasajes, Horarios y Precios de Aviatoor. Compra tu Pasaje Online. Bogota, Neiva, Florencia, Pitalito, Popayan.">
<meta name="google-site-verification" content="VSIQLGVVMJj026dPaw4rZVFZj2fLsQTlFLM8x4Ab1fg"/>

<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->

<meta name="robots" content="index, follow">
<!-- Schema.org markup for Google+ -->
<meta itemprop="name" content="Compra tu Pasaje de Bus en Aviatoor">
<meta itemprop="description" content="Pasajes de Bus Aviatoor, Pasajes, Horarios y Precios de Aviatoor. Compra tu Pasaje Online. Bogota, Neiva, Florencia, Pitalito, Popayan.">
<meta itemprop="image" content="img/icono.png">
<link rel="author" href="https://plus.google.com/+Pinbus00/posts">
<link rel="publisher" href="https://plus.google.com/+Pinbus00/">
<!-- Open Graph data Facebook-->
<meta property="og:url" content="?" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Compra tu Pasaje de Bus en Aviatoor" />
<meta property="og:description" content="Pasajes de Bus Aviatoor, Pasajes, Horarios y Precios de Aviatoor. Compra tu Pasaje Online. Bogota, Neiva, Florencia, Pitalito, Popayan."/>
<meta property="og:image" content="img/icono.png">
<!-- Twitter Card data -->
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@pin_bus">
<meta name="twitter:creator" content="@pin_bus">
<meta name="twitter:title" content="Compra tu Pasaje de Bus en Aviatoor">
<meta name="twitter:description" content="Pasajes de Bus Aviatoor, Pasajes, Horarios y Precios de Aviatoor. Compra tu Pasaje Online. Bogota, Neiva, Florencia, Pitalito, Popayan.">
<meta name="twitter:image" content="img/icono.png">

  <!-- Styles -->
  <link rel="canonical" href="?" />
  <link rel="icon" type="image/png" href="img/icono.png?v=983e1f7097cb89273880d863419887ea">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet">  
  <link rel="stylesheet" href="css/chr.min.css">

  


  <!-- End Visual Website Optimizer Asynchronous Code -->
  <script src="js/ajax.js"></script>
</head>

<body class="internal-bg  body-offcanvas" style="">
  
   <!-- End Google Tag Manager (noscript) -->
   <div id="app">
      <a href="#main" class="skip-main hide">Skip to main content</a> 
      <header class="clearfix">
         <nav class="navbar navbar-inverse navbar-static-top navbar-blue">
            <div class="container">
               <div class="row">
                  <div class="navbar-header col-md-3 col-lg-2"><button type="button" data-toggle="offcanvas" data-target="#js-bootstrap-offcanvas" class="navbar-toggle offcanvas-toggle pull-right"><span class="sr-only">Toggle navigation</span> <span><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></span></button> <a href="https://www.Aerotour.com.co" class="navbar-brand"><img src="img/AER.png?v=6686dd806ebd656325d0a195fdfe0265" alt="Pinbus.com"></a></div>
                  <div class="hidden-xs hidden-sm col-md-9 col-lg-10">
                     <div id="search_module">
                     <form action="buscar.php" method="GET">
      <div class="row">
         <div class="col-xs-12 col-md-10">
            <div class="row">
               <div class="col-xs-12 col-sm-6 col-md-3">
                  <div class="v-autocomplete" tabindex="1">
                     <div class="v-autocomplete-input-group"><input id="txtOrigen" name="txtOrigen" value="<?php echo $_GET['txtOrigen']; ?>" type="search" placeholder="Ciudad de Origen" class="form-control control-map" onkeyup="mostrarOrigen(this.value)" onclick="this.value=''"></div>
                     <input type="hidden" id="txtOrigenOculto" value="<?php echo $_GET['id_origen']; ?>" name="id_origen"/>
                     <!---->
                     <div class="v-autocomplete-list" id='respuestaOrigen'>
                        
                    </div>
                  </div>
               </div>
               <div class="col-xs-12 col-sm-6 col-md-3">
                  <div class="v-autocomplete" tabindex="2">
                     <div class="v-autocomplete-input-group"><input id="txtDestino" name="txtDestino" value="<?php echo $_GET['txtDestino']; ?>" type="search" placeholder="Ciudad de Destino" class="form-control control-map" onkeyup="mostrarDestino(document.getElementById('txtOrigenOculto').value,this.value)" onclick="this.value=''"></div>
                     <input type="hidden" id="txtDestinoOculto" name="id_destino" value="<?php echo $_GET['id_destino']; ?>" />
                     <!---->
                     <div class="v-autocomplete-list" id='respuestaDestino'>
                        
                    </div>
                  </div>
               </div>
               <div class="col-xs-6 col-md-3 col-sm-date">
                  <div class="vdp-datepicker" tabindex="3">
                     <div class="">
                        <!----> <input type="text" id="txtFechaSalida" name="salida" placeholder="Salida" value="<?php echo $_GET['salida']; ?>" required="required" readonly="readonly" class="form-control control-calendar"> <!---->
                     </div>
                  </div>
               </div>
               <div class="col-xs-6 col-md-3 col-sm-date">
                  <div class="vdp-datepicker" tabindex="4">
                     <div class="">
                        <!----> <input onClick="regreso(document.getElementById('txtFechaSalida').value)" type="text" id="txtFechaRegreso" value="<?php echo $_GET['regreso']; ?>" name="regreso" placeholder="Regreso (opcional)" clear-button="true" readonly="readonly" class="form-control control-calendar"> <!---->
                     </div>
                     
                  </div>
               </div>
            </div>
         </div>
         <div class="col-xs-12 col-sm-6 col-md-2"><button analytics-on="click" analytics-event="busqueda-pinbus" analytics-category="Content Actions" id="btn_buscar" type="submit" tabindex="6" class="btn btn-primary btn-block gtm-buscar">Buscar</button></div>
      </div>
   </form>
                     </div>
                  </div>
               </div>
            </div>
         </nav>
      </header>
         <div class="breadcrumb-wrapper">
            <div class="container">
               <div class="row">
                  <div class="col-xs-12">
                     <ol class="breadcrumb">
                        <li><a href="https://www.Aerotour.com.co">Búsqueda de Viajes</a></li>
                        <li class="active">
                           Resultado de búsqueda y Selección de Silla
                        </li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
         <div class="internal-page no-padding-top">
            <div class="row loading-flex" style="display: none;">
               <div class="col-md-12 text-center loading">
                  <img src="img/AER.png" alt="" class="center-block"> 
                  <div class="spinner">
                     <div class="bounce1"></div>
                     <div class="bounce2"></div>
                     <div class="bounce3"></div>
                  </div>
                  <h4>Preparando formulario de pasajeros y pagos…</h4>
               </div>
            </div>
            <div class="top-searcher" style="">
               <div class="container">
                  <div class="row">
                     <div class="col-sm-12 trip-title">
                         <?php 
                         $origen=explode(",",$_GET['txtOrigen']);
                         $origen=explode(")",$origen[0]);
                         $destino=explode(",",$_GET['txtDestino']);
                         $destino=explode(")",$destino[0]);
                         include_once 'funciones.php';
                         ?>
                        <h2>Salida de <strong><?php echo $origen[1];?></strong> a <strong><?php echo $destino[1];?></strong></h2>
                        <label class="label label-departure"><?php echo fechaEntexto($_GET['salida']); ?></label>
                        <?php
                        if(isset($_GET['regreso']) && $_GET['regreso']!=""){
                            ?>
                            <br/><br/><br/>
                            <h2>Regreso de <strong><?php echo $destino[1];?></strong> a <strong><?php echo $origen[1];?></strong></h2>
                            <label class="label label-departure"><?php echo fechaEntexto($_GET['regreso']); ?></label>
                            <?php
                        }
                        ?>

                     </div>
                  </div>
               </div>
            </div>
            <div class="container">
               <div class="row" style="display: none;">
                  <div class="col-md-12 text-center loading">
                     <img src="img/bus.gif" alt="" class="center-block"> 
                     <h4>Estamos buscando las mejores ofertas…</h4>
                  </div>
               </div>
               <!----> 
               <div class="row row-5" style="">
                  <div class="col-5 col-md-2 hidden-sm hidden-xs">
                     <div class="filter-list">
                        <div class="filter-box results"><strong>6</strong> Resultados
                        </div>
                        <div class="filter-box">
                           <h3>Empresas de Transporte</h3>
                           <ul>
                           <li>
                                 <div class="checkbox"><label><input type="checkbox" checked value="Aerotour"> Aerotour
                                    
                                 </div>
                              </li>
                              <li>
                                 <div class="checkbox"><label><input type="checkbox" checked value="Otras"> Otras empresas
                                    
                                 </div>
                              </li>
                           </ul>
                        </div>
                        <div class="filter-box">
                           <h3>Tipos de Avión</h3>
                           <ul>
                              <li>
                                 <div class="checkbox"><label><input type="checkbox" value="Navette "> Navette 
                                    
                                 </div>
                              </li>
                              <li>
                                 <div class="checkbox"><label><input type="checkbox" value="Navette G7"> Navette G7
                                    
                                 </div>
                              </li>
                              <li>
                                 <div class="checkbox"><label><input type="checkbox" value="Navette XL"> Navette XL
                                    
                                 </div>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="col-5 col-md-10">
                     <div class="order-travels form-inline">
                        <div class="form-group">
                           <label>Ordenar por</label> 
                           <select class="form-control">
                              <option value="temprano">
                                 Salida más temprana
                              </option>
                              <option value="tarde">
                                 Salida más tarde
                              </option>
                              <option value="menor">
                                 Menor precio
                              </option>
                              <option value="mayor">
                                 Mayor precio
                              </option>
                              <option value="A-Z">
                                 Empresa A-Z
                              </option>
                              <option value="Z-A">
                                 Empresa Z-A
                              </option>
                           </select>
                        </div>
                     </div>
                     <div class="trip-results" id="resultados">
                        <div class="alert alert-danger" style="display: none;">
                           No hemos podido reservar la silla, por favor escoge una diferente
                        </div>
                        <?php
                            require_once 'conexion.php';
                            $con=Conexion::conectar();
                            $con->set_charset('utf8');

                            $id_origen=$con->real_escape_string($_GET['id_origen']);
                            $id_destino=$con->real_escape_string($_GET['id_destino']);

                            $salida=$con->real_escape_string($_GET['salida']);
                            $regreso=$con->real_escape_string($_GET['regreso']);

                            $resultadoIda=$con->query("call sp_vuelos_ida('$salida','$id_origen','$id_destino')");
                            $resultadoRegreso=$con->query("call sp_vuelos_ida('$regreso','$id_origen','$id_destino')");
                        
                        while($campos=$resultadoIda->fetch_object()){
                            ?>

                        <div class="trip-result">
                           <?php
                           if($campos->disponibles<=10){
                           ?>
                        <div class="ribbon"><span>Últimas Sillas</span></div>
                        <?php
                           }
                           ?>

                           <div class="row">
                              <div class="col-md-12">
                                 <div class="row trip-row">
                                    <div class="col-sm-12 col-md-2 travel-operator"><img src="img/<?php echo $campos->codigo_aerolinea; ?>.png" alt="Aerotour Web" class="operator-logo"></div>
                                    <div class="col-sm-12 col-md-8 travel-info">
                                       <div class="row">
                                          <div class="col-xs-4 col-md-2 travel-info-box">
                                             <span>Salida</span> 
                                             <h4><?php echo substr($campos->hora_salida,0,5); ?></h4>
                                             <span class="subtitle">22, sep.</span>
                                          </div>
                                          <div class="col-xs-4 col-md-2 travel-info-box">
                                             <span>Llegada</span> 
                                             <h4><?php echo substr($campos->hora_llegada,0,5); ?></h4>
                                             <span class="subtitle">23, sep.</span>
                                          </div>
                                          <div class="col-xs-4 col-md-3 travel-info-box">
                                             <span>Sillas</span> 
                                             <h4><?php echo $campos->disponibles; ?> </h4>
                                             <span class="subtitle">Disponibles</span>
                                          </div>
                                          <div class="col-xs-12 col-md-5 travel-info-box travel-services">
                                             <div class="travel-services">
                                                <span class="bus-service-name">Navette G7</span> 
                                                <div class="bus-service-container">
                                                   <a href="#" data-toggle="tooltip" data-placement="top" title="Baños"><img src="img/operadoras/bathroom.png"></a> <a href="#" data-toggle="tooltip" data-placement="top" title="Sillas Reclinables"><img src="img/operadoras/reclining_chairs.png"></a> <!----> <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Aire Acondicionado"><img src="img/operadoras/air_conditioner.png"></a> <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="TV Ambiental"><img src="img/operadoras/tv.png"></a> <a href="#" data-toggle="tooltip" data-placement="top" title="Pantallas Individuales"><img src="img/operadoras/individual_screen.png"></a> <a href="#" data-toggle="tooltip" data-placement="top" title="Dos Conductores"><img src="img/operadoras/multiple_drivers.png"></a> <a href="#" data-toggle="tooltip" data-placement="top" title="Toma Corriente"><img src="img/operadoras/socket_energy.png"></a> <a href="#" data-toggle="tooltip" data-placement="top" title="Wifi"><img src="img/operadoras/wifi.png"></a>
                                                </div>
                                                <span class="subtitle">Servicios</span>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-sm-12 col-md-2 booking trip-price text-center">
                                       <h4>$<?php echo number_format($campos->valor); ?></h4>
                                       <button class="btn btn-primary">Ver Sillas</button> <!----> 
                                       <p class="small">Valor Trayecto por Persona</p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!---->
                        </div>
                        <?php
                        }
                        ?>

                        <div class="trip-result">
                            <div class="ribbon" style="display: none;"><span>Últimas Sillas</span></div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row trip-row">
                                        <div class="col-sm-12 col-md-2 travel-operator"><img src="img/AER.png" alt="Aerotour Web" class="operator-logo"></div>
                                        <div class="col-sm-12 col-md-8 travel-info">
                                        <div class="row">
                                            <div class="col-xs-4 col-md-2 travel-info-box">
                                                <span>Salida</span> 
                                                <h4>15:00</h4>
                                                <span class="subtitle">22, sep.</span>
                                            </div>
                                            <div class="col-xs-4 col-md-2 travel-info-box">
                                                <span>Llegada</span> 
                                                <h4>03:15</h4>
                                                <span class="subtitle">23, sep.</span>
                                            </div>
                                            <div class="col-xs-4 col-md-3 travel-info-box">
                                                <span>Sillas</span> 
                                                <h4>34 </h4>
                                                <span class="subtitle">Disponibles</span>
                                            </div>
                                            <div class="col-xs-12 col-md-5 travel-info-box travel-services">
                                                <div class="travel-services">
                                                    <span class="bus-service-name">Navette G7</span> 
                                                    <div class="bus-service-container">
                                                    <a href="#" data-toggle="tooltip" data-placement="top" title="Baños"><img src="img/operadoras/bathroom.png"></a> <a href="#" data-toggle="tooltip" data-placement="top" title="Sillas Reclinables"><img src="img/operadoras/reclining_chairs.png"></a> <!----> <a href="#" data-toggle="tooltip" data-placement="top" title="Aire Acondicionado"><img src="img/operadoras/air_conditioner.png"></a> <a href="#" data-toggle="tooltip" data-placement="top" title="TV Ambiental"><img src="img/operadoras/tv.png"></a> <a href="#" data-toggle="tooltip" data-placement="top" title="Pantallas Individuales"><img src="img/operadoras/individual_screen.png"></a> <a href="#" data-toggle="tooltip" data-placement="top" title="Dos Conductores"><img src="img/operadoras/multiple_drivers.png"></a> <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Toma Corriente"><img src="img/operadoras/socket_energy.png"></a> <a href="#" data-toggle="tooltip" data-placement="top" title="Wifi"><img src="img/operadoras/wifi.png"></a>
                                                    </div>
                                                    <span class="subtitle">Servicios</span>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="col-sm-12 col-md-2 booking trip-price text-center">
                                        <h4>$82,000</h4>
                                        <!----> <button class="btn btn-primary">Ocultar Sillas</button> 
                                        <p class="small">Valor Trayecto por Persona</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="busmap-container">
                                <!----> 
                                <div class="col-xs-12 col-sm-12 col-md-12 busmap-wrapper">
                                    <div class="row">
                                        <div class="col-sm-9 col-md-9">
                                        <div class="row">
                                            <div class="hidden-xs hidden-sm col-md-3 border-dotted-right">
                                                <div class="seat-description bus-picture-wrapper">
                                                    <div class="col-md-12">
                                                    <h5 class="seat seat-available">Disponible</h5>
                                                    </div>
                                                    <div class="col-md-12">
                                                    <h5 class="seat seat-selected">Seleccionado</h5>
                                                    </div>
                                                    <div class="col-md-12">
                                                    <h5 class="seat seat-taken">Ocupado</h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
                                                <h3 class="bus-layout-title">NAVETTE G7 CARROCERIA </h3>
                                                <div class="bus-layout">
                                                    <table class="table">
                                                    <tr class="bus-wheel">
                                                        <td>
                                                            
                                                        </td>
                                                        <td>
                                                        <span class="wheel-icon"></span>
                                                        </td>
                                                        <td>
                                                            <!---->
                                                        </td>
                                                        <td>
                                                        <span class="wheel-icon"></span>
                                                        </td>
                                                        <td>
                                                            <!---->
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div data-toggle="" data-placement="top" title="$0" class="place">
                                                                <span class=""></span> <!---->
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="" data-placement="top" title="$0" class="place pasillo">
                                                                <span class="corredor"></span> <!---->
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="" data-placement="top" title="$0" class="place pasillo">
                                                                <span class="corredor corredor-medio"></span> <!---->
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="" data-placement="top" title="$0" class="place pasillo">
                                                                <span class="corredor corredor-medio"></span> <!---->
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="" data-placement="top" title="$0" class="place">
                                                                <span class=""></span> <!---->
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="$82,000" class="place seat seat-available"><span class=""></span> <span class="seat-number">01</span></div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="$82,000" class="place seat seat-available"><span class=""></span> <span class="seat-number">02</span></div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="" data-placement="top" title="$0" class="place tv">
                                                                <span class="corredor-medio"></span> <!---->
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="" class="place seat seat-selected" data-original-title="$82,000"><span class="corredor-medio"></span> <span class="seat-number">04</span></div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="$82,000" class="place seat seat-occupied"><span class=""></span> <span class="seat-number">03</span></div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="$82,000" class="place seat seat-occupied"><span class=""></span> <span class="seat-number">05</span></div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="$82,000" class="place seat seat-occupied"><span class=""></span> <span class="seat-number">06</span></div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="" data-placement="top" title="$0" class="place pasillo">
                                                                <span class="corredor corredor-medio"></span> <!---->
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="" class="place seat seat-selected" data-original-title="$82,000"><span class="corredor-medio"></span> <span class="seat-number">08</span></div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="$82,000" class="place seat seat-occupied"><span class=""></span> <span class="seat-number">07</span></div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="" class="place seat seat-available" data-original-title="$82,000"><span class=""></span> <span class="seat-number">09</span></div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="" class="place seat seat-available" data-original-title="$82,000"><span class=""></span> <span class="seat-number">10</span></div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="" data-placement="top" title="$0" class="place pasillo">
                                                                <span class="corredor corredor-medio"></span> <!---->
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="" class="place seat seat-selected" data-original-title="$82,000"><span class="corredor-medio"></span> <span class="seat-number">12</span></div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="" class="place seat seat-available" data-original-title="$82,000"><span class=""></span> <span class="seat-number">11</span></div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="$82,000" class="place seat seat-occupied"><span class=""></span> <span class="seat-number">13</span></div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="$82,000" class="place seat seat-occupied"><span class=""></span> <span class="seat-number">14</span></div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="" data-placement="top" title="$0" class="place pasillo">
                                                                <span class="corredor corredor-medio"></span> <!---->
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="" class="place seat seat-occupied" data-original-title="$82,000"><span class="corredor-medio"></span> <span class="seat-number">16</span></div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="$82,000" class="place seat seat-occupied"><span class=""></span> <span class="seat-number">15</span></div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="$82,000" class="place seat seat-available"><span class=""></span> <span class="seat-number">17</span></div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="" class="place seat seat-available" data-original-title="$82,000"><span class=""></span> <span class="seat-number">18</span></div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="" data-placement="top" title="$0" class="place tv">
                                                                <span class="corredor-medio"></span> <!---->
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="" class="place seat seat-selected" data-original-title="$82,000"><span class="corredor-medio"></span> <span class="seat-number">20</span></div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="" class="place seat seat-available" data-original-title="$82,000"><span class=""></span> <span class="seat-number">19</span></div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="$82,000" class="place seat seat-available"><span class=""></span> <span class="seat-number">21</span></div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="" class="place seat seat-available" data-original-title="$82,000"><span class=""></span> <span class="seat-number">22</span></div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="" data-placement="top" title="$0" class="place pasillo">
                                                                <span class="corredor corredor-medio"></span> <!---->
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="" class="place seat seat-selected" data-original-title="$82,000"><span class="corredor-medio"></span> <span class="seat-number">24</span></div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="" class="place seat seat-available" data-original-title="$82,000"><span class=""></span> <span class="seat-number">23</span></div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="$82,000" class="place seat seat-available"><span class=""></span> <span class="seat-number">25</span></div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="$82,000" class="place seat seat-available"><span class=""></span> <span class="seat-number">26</span></div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="" data-placement="top" title="$0" class="place pasillo">
                                                                <span class="corredor corredor-medio"></span> <!---->
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="" class="place seat seat-selected" data-original-title="$82,000"><span class="corredor-medio"></span> <span class="seat-number">28</span></div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="" class="place seat seat-available" data-original-title="$82,000"><span class=""></span> <span class="seat-number">27</span></div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="$82,000" class="place seat seat-available"><span class=""></span> <span class="seat-number">29</span></div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="$82,000" class="place seat seat-available"><span class=""></span> <span class="seat-number">30</span></div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="" data-placement="top" title="$0" class="place tv">
                                                                <span class="corredor-medio"></span> <!---->
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="$82,000" class="place seat seat-available"><span class="corredor-medio"></span> <span class="seat-number">32</span></div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="$82,000" class="place seat seat-available"><span class=""></span> <span class="seat-number">31</span></div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="$82,000" class="place seat seat-available"><span class=""></span> <span class="seat-number">33</span></div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="$82,000" class="place seat seat-available"><span class=""></span> <span class="seat-number">34</span></div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="" data-placement="top" title="$0" class="place pasillo">
                                                                <span class="corredor corredor-medio"></span> <!---->
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="" class="place seat seat-available" data-original-title="$82,000"><span class="corredor-medio"></span> <span class="seat-number">36</span></div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="$82,000" class="place seat seat-available"><span class=""></span> <span class="seat-number">35</span></div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="$82,000" class="place seat seat-available"><span class=""></span> <span class="seat-number">37</span></div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="$82,000" class="place seat seat-available"><span class=""></span> <span class="seat-number">38</span></div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="" data-placement="top" title="$0" class="place pasillo">
                                                                <span class="corredor corredor-medio"></span> <!---->
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="" class="place seat seat-available" data-original-title="$82,000"><span class="corredor-medio"></span> <span class="seat-number">40</span></div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="$82,000" class="place seat seat-available"><span class=""></span> <span class="seat-number">39</span></div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="$82,000" class="place seat seat-available"><span class=""></span> <span class="seat-number">41</span></div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="tooltip" data-placement="top" title="$82,000" class="place seat seat-available"><span class=""></span> <span class="seat-number">42</span></div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="" data-placement="top" title="$0" class="place pasillo">
                                                                <span class="corredor corredor-medio"></span> <!---->
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="" data-placement="top" title="$0" class="place wc">
                                                                <span class="corredor-medio"></span> <!---->
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="" data-placement="top" title="$0" class="place wc">
                                                                <span class=""></span> <!---->
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div data-toggle="" data-placement="top" title="" class="place">
                                                                <span class=""></span> <!---->
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="" data-placement="top" title="" class="place">
                                                                <span class=""></span> <!---->
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="" data-placement="top" title="" class="place">
                                                                <span class=""></span> <!---->
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="" data-placement="top" title="" class="place">
                                                                <span class=""></span> <!---->
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div data-toggle="" data-placement="top" title="" class="place">
                                                                <span class=""></span> <!---->
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="col-sm-3 col-md-3 passengers-list">
                                        <h4>Sillas Seleccionadas</h4>
                                        <div class="seat-description">
                                            <div class="col-md-12">
                                                <div>
                                                    <div class="row reserved-seat">
                                                    <div class="col-md-12">
                                                        <div class="row row-flex">
                                                            <div class="place seat"><span class="seat-number">04</span></div>
                                                            <div class="price">
                                                                <h4>$82,000</h4>
                                                                <span>Valor Trayecto por Persona</span>
                                                            </div>
                                                            <a class="btn-remove-seat pull-right"><i class="fa fa-trash-o"></i></a>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="row reserved-seat">
                                                    <div class="col-md-12">
                                                        <div class="row row-flex">
                                                            <div class="place seat"><span class="seat-number">08</span></div>
                                                            <div class="price">
                                                                <h4>$82,000</h4>
                                                                <span>Valor Trayecto por Persona</span>
                                                            </div>
                                                            <a class="btn-remove-seat pull-right"><i class="fa fa-trash-o"></i></a>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="row reserved-seat">
                                                    <div class="col-md-12">
                                                        <div class="row row-flex">
                                                            <div class="place seat"><span class="seat-number">12</span></div>
                                                            <div class="price">
                                                                <h4>$82,000</h4>
                                                                <span>Valor Trayecto por Persona</span>
                                                            </div>
                                                            <a class="btn-remove-seat pull-right"><i class="fa fa-trash-o"></i></a>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="row reserved-seat">
                                                    <div class="col-md-12">
                                                        <div class="row row-flex">
                                                            <div class="place seat"><span class="seat-number">20</span></div>
                                                            <div class="price">
                                                                <h4>$82,000</h4>
                                                                <span>Valor Trayecto por Persona</span>
                                                            </div>
                                                            <a class="btn-remove-seat pull-right"><i class="fa fa-trash-o"></i></a>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="row reserved-seat">
                                                    <div class="col-md-12">
                                                        <div class="row row-flex">
                                                            <div class="place seat"><span class="seat-number">24</span></div>
                                                            <div class="price">
                                                                <h4>$82,000</h4>
                                                                <span>Valor Trayecto por Persona</span>
                                                            </div>
                                                            <a class="btn-remove-seat pull-right"><i class="fa fa-trash-o"></i></a>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="row reserved-seat">
                                                    <div class="col-md-12">
                                                        <div class="row row-flex">
                                                            <div class="place seat"><span class="seat-number">28</span></div>
                                                            <div class="price">
                                                                <h4>$82,000</h4>
                                                                <span>Valor Trayecto por Persona</span>
                                                            </div>
                                                            <a class="btn-remove-seat pull-right"><i class="fa fa-trash-o"></i></a>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                                <!----> 
                                                <div class="well empty text-center">
                                                    <h4><span>Ha seleccionado la cantidad </span><strong>maxima </strong>permitida de asientos por este operador
                                                    </h4>
                                                </div>
                                            </div>
                                            <div class="col-md-12"><a class="btn btn-primary btn-block">Continuar <i class="fa fa-angle-double-right"></i></a></div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            </div>
                        
                        <div class="trip-result">
                           <div class="ribbon" style="display: none;"><span>Últimas Sillas</span></div>
                           <div class="row">
                              <div class="col-md-12">
                                 <div class="row trip-row">
                                    <div class="col-sm-12 col-md-2 travel-operator"><img src="img/LAT.png" alt="Aerotour Web" class="operator-logo"></div>
                                    <div class="col-sm-12 col-md-8 travel-info">
                                       <div class="row">
                                          <div class="col-xs-4 col-md-2 travel-info-box">
                                             <span>Salida</span> 
                                             <h4>20:00</h4>
                                             <span class="subtitle">22, sep.</span>
                                          </div>
                                          <div class="col-xs-4 col-md-2 travel-info-box">
                                             <span>Llegada</span> 
                                             <h4>07:00</h4>
                                             <span class="subtitle">23, sep.</span>
                                          </div>
                                          <div class="col-xs-4 col-md-3 travel-info-box">
                                             <span>Sillas</span> 
                                             <h4>42 </h4>
                                             <span class="subtitle">Disponibles</span>
                                          </div>
                                          <div class="col-xs-12 col-md-5 travel-info-box travel-services">
                                             <div class="travel-services">
                                                <span class="bus-service-name">Navette G7</span> 
                                                <div class="bus-service-container">
                                                   <a href="#" data-toggle="tooltip" data-placement="top" title="Baños"><img src="img/operadoras/bathroom.png"></a> <a href="#" data-toggle="tooltip" data-placement="top" title="Sillas Reclinables"><img src="img/operadoras/reclining_chairs.png"></a> <!----> <a href="#" data-toggle="tooltip" data-placement="top" title="Aire Acondicionado"><img src="img/operadoras/air_conditioner.png"></a> <a href="#" data-toggle="tooltip" data-placement="top" title="TV Ambiental"><img src="img/operadoras/tv.png"></a> <a href="#" data-toggle="tooltip" data-placement="top" title="Pantallas Individuales"><img src="img/operadoras/individual_screen.png"></a> <a href="#" data-toggle="tooltip" data-placement="top" title="Dos Conductores"><img src="img/operadoras/multiple_drivers.png"></a> <a href="#" data-toggle="tooltip" data-placement="top" title="Toma Corriente"><img src="img/operadoras/socket_energy.png"></a> <a href="#" data-toggle="tooltip" data-placement="top" title="Wifi"><img src="img/operadoras/wifi.png"></a>
                                                </div>
                                                <span class="subtitle">Servicios</span>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-sm-12 col-md-2 booking trip-price text-center">
                                       <h4>$82,000</h4>
                                       <button class="btn btn-primary">Ver Sillas</button> <!----> 
                                       <p class="small">Valor Trayecto por Persona</p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!---->
                        </div>

                     </div>
                  </div>
               </div>
               <div class="row" style="display: none;">
                  <div class="col-md-12">
                     <div class="empty-list">
                        <img src="img/empty-list.png" alt=""> 
                        <h2>No se encontraron rutas para esta fecha</h2>
                        <p>Por favor intente de nuevo con otra fecha.</p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
   </div>
   <footer id="footer">
      <div class="container">
         <div class="row">
            <div class="col-sm-5 pinbus_logo_social">
               <div class="social_links">
                  <a target="_blank" href="#" class="facebook boxed-icon icon-2x" title=""><i class="fa fa-facebook"></i></a>
                  <a target="_blank" href="#" class="twitter boxed-icon icon-1x" title=""><i class="fa fa-twitter"></i></a>
                  <a target="_blank" href="#" class="google boxed-icon icon-1x" title=""><i class="fa fa-google-plus"></i></a>
                  <a target="_blank" href="#" class="instagram boxed-icon icon-1x" title=""><i class="fa fa-instagram"></i></a>
                  <div class="clearfix"></div>
               </div>
            </div>
            <div class="col-xs-6 col-xs-12 col-sm-2 ">
               <img src="img/tsconfig.png?v=95aab593e45cb88e880411d8341d3d8e" alt="Pinbus Redes Sociales" class="img-responsive center-block">
            </div>
            <div class="col-xs-6 col-xs-12 col-sm-3 pinbus_logo_social">
               <img src="img/AER.png?v=9ea45cff778ed69b79374f03fc80c42a" alt="Pinbus Redes Sociales" class="img-responsive center-block">
            </div>
         </div>
      </div>
   </footer>
   <!-- Scripts -->
   <!-- OPTIMIZE CSS DELIVERY -->
   <script>
      WebFontConfig={google:{families:['Montserrat:300,400,700','Fira+Sans:400,300,500,700']}};(function(){var wf=document.createElement('script');wf.src=('https:'==document.location.protocol?'https':'http')+'://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';wf.type='text/javascript';wf.async='true';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(wf,s)})();
   </script>
   <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-56403230-1', 'auto');
      ga('send', 'pageview');
   </script>
   <script src="//load.sumome.com/" data-sumo-site-id="62512569b57bd04c77a514bd80fb6877f0e2ac2d2998d9971c7ed6d88ead5557" async="async"></script>
   <script type="text/javascript">
      window._mfq = window._mfq || [];
      (function() {
          var mf = document.createElement("script");
          mf.type = "text/javascript"; mf.async = true;
          mf.src = "//cdn.mouseflow.com/projects/454b85ea-2e2d-41b9-93c4-fb7a41b8e7ff.js";
          document.getElementsByTagName("head")[0].appendChild(mf);
      })();
   </script>
   <script type="text/javascript" src="https://www.googleadservices.com/pagead/conversion_async.js" charset="utf-8"></script>
   <script src="https://www.Aerotour.com.co/assets/js/pinbus.min.js?v=6ce19ca2e27f901d4fc6f4632ee0bb40"></script><script type="text/javascript" id="">!function(b,e,f,g,a,c,d){b.fbq||(a=b.fbq=function(){a.callMethod?a.callMethod.apply(a,arguments):a.queue.push(arguments)},b._fbq||(b._fbq=a),a.push=a,a.loaded=!0,a.version="2.0",a.queue=[],c=e.createElement(f),c.async=!0,c.src=g,d=e.getElementsByTagName(f)[0],d.parentNode.insertBefore(c,d))}(window,document,"script","https://connect.facebook.net/en_US/fbevents.js");fbq("init","869762026540993");fbq("set","agent","tmgoogletagmanager","869762026540993");
      fbq("track","Search",{cityFrom:"MEDELLIN",departingArrivalDate:"2018-09-22",returningArrivalDate:"",targetCity:"NEIVA",numberPassenger:"1"});
   </script><script type="text/javascript" id="">!function(b,e,f,g,a,c,d){b.fbq||(a=b.fbq=function(){a.callMethod?a.callMethod.apply(a,arguments):a.queue.push(arguments)},b._fbq||(b._fbq=a),a.push=a,a.loaded=!0,a.version="2.0",a.queue=[],c=e.createElement(f),c.async=!0,c.src=g,d=e.getElementsByTagName(f)[0],d.parentNode.insertBefore(c,d))}(window,document,"script","https://connect.facebook.net/en_US/fbevents.js");fbq("init","869762026540993");fbq("set","agent","tmgoogletagmanager","869762026540993");
      fbq("track","Search",{cityFrom:"MEDELLIN",departingArrivalDate:"2018-09-22",returningArrivalDate:"",targetCity:"NEIVA",numberPassenger:"1"});
   </script>
   <script type="text/javascript" id="">!function(b,e,f,g,a,c,d){b.fbq||(a=b.fbq=function(){a.callMethod?a.callMethod.apply(a,arguments):a.queue.push(arguments)},b._fbq||(b._fbq=a),a.push=a,a.loaded=!0,a.version="2.0",a.queue=[],c=e.createElement(f),c.async=!0,c.src=g,d=e.getElementsByTagName(f)[0],d.parentNode.insertBefore(c,d))}(window,document,"script","https://connect.facebook.net/en_US/fbevents.js");fbq("init","869762026540993");fbq("track","PageView");</script>
   <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=869762026540993&amp;ev=PageView&amp;noscript=1"></noscript>
   <script>
      jQuery(function($){
        $('#flash-overlay-modal').modal();
      
        // Disable back button
        window.history.pushState(null, "", window.location.href);
        window.onpopstate = function () {
          window.history.pushState(null, "", window.location.href);
        };
      
      
      }).noConflict;
   </script>
   <iframe id="sumome-jquery-iframe" title="Sumo Hidden Content" style="display: none;"></iframe><a href="javascript:void(0);" title="Sumo" style="background-color: rgb(0, 115, 183); border-radius: 3px 0px 0px 3px; box-shadow: rgba(0, 0, 0, 0.2) 0px 4px 10px; position: fixed; z-index: 2147483647; padding: 0px; width: 44px; height: 40px; text-indent: -10000px; opacity: 1; display: block !important; top: 40px; right: -40px;"><span style="position: absolute; left: -10000px; top: auto; width: 1px; height: 1px; overflow: hidden; border-radius: 3px 0px 0px 3px; margin-left: 4px; margin-right: 0px;">Sumo</span><span style="display: block; width: 40px; height: 40px; background: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAMAAADXqc3KAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA3hpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDoxZDQ2MjI4YS03NWY2LTRkZTQtOGJjYy1hODc1NjRkMjYxYTUiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6RDQ3MUVFMDFFMjVDMTFFNjlFQjhBRjdGODU5MDJBMDUiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6RDQ3MUVFMDBFMjVDMTFFNjlFQjhBRjdGODU5MDJBMDUiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKE1hY2ludG9zaCkiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDoxZDQ2MjI4YS03NWY2LTRkZTQtOGJjYy1hODc1NjRkMjYxYTUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MWQ0NjIyOGEtNzVmNi00ZGU0LThiY2MtYTg3NTY0ZDI2MWE1Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+8JtvywAAAKhQTFRFzOPxSJvLA3W4Mo7FBna5w97u8vj7EHy8a67VhbzdsdTpVaLPh73d9/v9C3m6QZfJbq/WXKbR3u32JIfB3ez1KorDir/eBHW4+/3+rtPoZarUG4K/LIvDDnu7ocvkf7nbdrTY9fr8E328WKPQO5PITJ3MPpXJstXptdbq+Pv9cbHXaq3VU6HOkMLgnMnjDHq62uv1/P3+6/T53Oz1cLDX/f7+AHO3////ptOZ5QAAADh0Uk5T/////////////////////////////////////////////////////////////////////////wA7XBHKAAAAmUlEQVR42sSRRxLCMAxFRSq9907ovebr/jdDMWYGxmZL3sIqbyFbJv4B/VlkVlerKALTD3G46Mx3AOR1UZ/TsDxW6W0gfYRNVfTCFu2AWpAMgMItMW+zQJU2UjWI29D0+e5KWNPMk+A9Om+B/UgOJyCuwMJCrpuziYkIsglfRByZ/XM3eXnBFEu1kpMpjq9dxQYp/OAXTwEGAB7Rc1xVnPemAAAAAElFTkSuQmCC&quot;) 8px 8px no-repeat white; margin-left: 4px; border-radius: 3px 0px 0px 3px; margin-right: 0px;"></span></a><iframe style="display: none;"></iframe>
   <div style="position:fixed;top:0;left:0;overflow:hidden;"><label for="focus_retriever" style="position:absolute;left:-1000px;">Focus Retriever</label><input style="position:absolute;left:-1000px;" type="text" name="focus_retriever" value="" id="focus_retriever" readonly="true"></div>
   <div class="sumome-image-sharer" style="width: 40px;">
      <a href="javascript:void(0);" class="sumome-image-sharer-facebook">
         <div></div>
      </a>
   </div>
   <div id="sumo_twilighter_div" class="sumo_highlighter_desktop" style="display: none; padding: 15px; background-color: rgb(240, 240, 240);">
      <textarea id="sumo_twilighter_input" class="sumo_twilighter_textarea"></textarea>
      <div class="sumo_twilighter_admin_highlight" style="display:none;">
         <a href="javascript:void(0);" style="color: rgb(125, 125, 125);">Save Highlight</a>
      </div>
      <div class="sumo_twilighter_admin" style="display:none;">
         <a href="javascript:void(0);" style="color: rgb(125, 125, 125);">Delete Highlight</a>
      </div>
      <br>
      <div class="sumo_twilighter_tweet">
         <a id="sumo_twilighter_btn" href="javascript:void(0);" alt="Tweet this" style="background-color: rgb(52, 132, 191);">
         <img class="btn-icon" src="//sumo.b-cdn.net/static/c6891b468b824630af272bcf862576f0b253972d/client/images/apps/331c6750-848e-4469-b1bb-bfbb4fa4cd99/twitter-white-60.png">
         <span class="btn-text" style="color: white;">Tweet</span>
         </a>
      </div>
      <div class="sumo_twilighter_fb">
         <a id="sumo_twilighter_btn_fb" href="javascript:void(0);" alt="Facebook Share This" style="background-color: rgb(76, 102, 164);">
         <img class="btn-icon" src="//sumo.b-cdn.net/static/c6891b468b824630af272bcf862576f0b253972d/client/images/apps/331c6750-848e-4469-b1bb-bfbb4fa4cd99/facebook-white-60.png">
         <span class="btn-text" style="color: white;">Share</span>
         </a>
      </div>
      <div class="sumo_twilighter_remaining" style="color: rgb(125, 125, 125);"></div>
   </div>
   <script>
  WebFontConfig={google:{families:['Roboto:300,400,700']}};(function(){var wf=document.createElement('script');wf.src=('https:'==document.location.protocol?'https':'http')+'://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';wf.type='text/javascript';wf.async='true';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(wf,s)})();
  </script>


 

  
 

  
  

  <script src="js/pinbus.min.js?v=5f780679a01d6e3609e4de04d4250472"></script>
  <!--FIN -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
   <script src="js/moment.min.js"></script>
   <script src="js/bootstrap-datetimepicker.min.js"></script>
   <script src="js/bootstrap-datetimepicker.es.js"></script>
   <script type="text/javascript">
   var hoy=new Date();
     $('#txtFechaSalida').datetimepicker({
          format: 'YYYY-MM-DD',
          pickTime: false
      });
      $('#txtFechaSalida').data("DatePicker").show();
   </script>
   <script type="text/javascript">
       $('#txtFechaRegreso').datetimepicker({
          format: 'YYYY-MM-DD',
          pickTime: false,
      });
      $('#txtFechaRegreso').data("DatePicker").show();
   </script>

  
</body>
</html>
