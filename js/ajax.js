function mostrarOrigen(texto){
    var xhttp = new XMLHttpRequest();//hacemos preguntas al servidor
    xhttp.onreadystatechange = function(){//propiedad que nos avisa cada vez que la pagina o el servidor tiene algun cambio
        if(this.readyState==4 && this.status==200){
            document.getElementById('respuestaOrigen').innerHTML=this.responseText;
        }//si hicimos la consulta y esa respuesta fue exitosa
    };
    /*propiedad readyState que tiene una pagina
    0 cuando la respuesta no ha sido inicializada
    1 cuando no nos hemos comunicado con el servidor  
    2 cuando recibimos esa respuesta
    3 cuando esa respuesta se esta procesando
    4 cuando ya tenemos la respuesta lista para ser usada
    */

    /*propiedad status, es el estado de esa respuesta
    200 cuando la respuesta fue completamente exitosa
    404 cuando no se encuentra ese archivo que vamos mas adelante a llamar*/
    xhttp.open('GET','origen.php?origen='+texto,true);//digo que metodo voy a usar para el envio de datos
    /*open consulta asincrona: por que metodo voy a enviar los datos, quien va a recibir los datos, se va a recargar la pagina?*/
    xhttp.send();
}

function mostrarDestino(origen,destino){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function(){
        if(this.readyState==4 && this.status==200){
            document.getElementById('respuestaDestino').innerHTML=this.responseText;
        }
    };
    xhttp.open('GET','origen.php?id_origen='+origen+'&destino='+destino,true);
    xhttp.send();
}

