
<!DOCTYPE html>
<html lang="es" itemscope itemtype="http://schema.org/WebPage">
<head>
  <title>AeroTour</title>

  <meta charset="utf-8">
<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="expires" content="0">

<!-- CSRF Token -->
<meta name="csrf-token" content="Q3MAREqJb3eFRWWrov32D7fPVrVaTnQ8lpsXHsuo">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Cache-control" content="no-cache" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="expires" content="0" />

<meta name="author" content="Aviatoor">
<meta name="description" content="Pasajes de Bus Aviatoor, Pasajes, Horarios y Precios de Aviatoor. Compra tu Pasaje Online. Bogota, Neiva, Florencia, Pitalito, Popayan.">
<meta name="google-site-verification" content="VSIQLGVVMJj026dPaw4rZVFZj2fLsQTlFLM8x4Ab1fg"/>

<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->

<meta name="robots" content="index, follow">
<!-- Schema.org markup for Google+ -->
<meta itemprop="name" content="Compra tu Pasaje de Bus en Aviatoor">
<meta itemprop="description" content="Pasajes de Bus Aviatoor, Pasajes, Horarios y Precios de Aviatoor. Compra tu Pasaje Online. Bogota, Neiva, Florencia, Pitalito, Popayan.">
<meta itemprop="image" content="img/icono.png">
<link rel="author" href="https://plus.google.com/+Pinbus00/posts">
<link rel="publisher" href="https://plus.google.com/+Pinbus00/">
<!-- Open Graph data Facebook-->
<meta property="og:url" content="?" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Compra tu Pasaje de Bus en Aviatoor" />
<meta property="og:description" content="Pasajes de Bus Aviatoor, Pasajes, Horarios y Precios de Aviatoor. Compra tu Pasaje Online. Bogota, Neiva, Florencia, Pitalito, Popayan."/>
<meta property="og:image" content="img/icono.png">
<!-- Twitter Card data -->
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@pin_bus">
<meta name="twitter:creator" content="@pin_bus">
<meta name="twitter:title" content="Compra tu Pasaje de Bus en Aviatoor">
<meta name="twitter:description" content="Pasajes de Bus Aviatoor, Pasajes, Horarios y Precios de Aviatoor. Compra tu Pasaje Online. Bogota, Neiva, Florencia, Pitalito, Popayan.">
<meta name="twitter:image" content="img/icono.png">

  <!-- Styles -->
  <link rel="canonical" href="?" />
  <link rel="icon" type="image/png" href="img/icono.png?v=983e1f7097cb89273880d863419887ea">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet">  
  <link rel="stylesheet" href="css/chr.min.css">

  


  <!-- End Visual Website Optimizer Asynchronous Code -->
<script src="js/ajax.js"></script>
</head>
<body class="home-bg  body-offcanvas">

  

  <a class="skip-main hide" href="#main">Skip to main content</a>

  <header class="clearfix">

    <nav class="navbar navbar-inverse navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle offcanvas-toggle pull-right" data-toggle="offcanvas" data-target="#js-bootstrap-offcanvas">
            <span class="sr-only">Toggle navigation</span>
            <span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </span>
          </button>

          <a class="navbar-brand" href="?">
            <img src="img/AER.png" alt="">
          </a>
        </div>
                <div class="navbar-offcanvas navbar-offcanvas-touch" id="js-bootstrap-offcanvas">
          <ul class="nav navbar-nav navbar-right">
    <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Nosotros <span class="caret"></span></a>
    <ul class="dropdown-menu">
      <li><a href="?/portafolio">Portafolio</a></li>
      <li><a href="?/organizacion">Nuestra Organizaci&oacute;n</a></li>
      
      
      <li><a href="?/agencias">Agencias y Terminales</a></li>
      <li><a href="https://correo.Aviatoor.com.co/owa/auth/logon.aspx?replaceCurrent=1&amp;url=https%3a%2f%2fcorreo.Aviatoor.com.co%2fowa%2f" target="_blank">Correo Corporativo</a></li>
      <li><a href="?/noticias">Noticias</a></li>
    </ul>
  </li>

  <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Club Viajero <span class="caret"></span></a>
    <ul class="dropdown-menu">
      <li><a href="?/bienvenidos">Bienvenidos</a></li>
      <li><a href="?/inscripcion">Inscripci&oacute;n</a></li>
      <li><a href="?/consulta_puntos">Consulta de Puntos</a></li>
      <li><a href="?/tabla_redencion">Tabla de Redención</a></li>
    </ul>
  </li>

  <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Cont&aacute;ctenos <span class="caret"></span></a>
    <ul class="dropdown-menu">
      <li><a href="?/contactenos">Atenci&oacute;n al Cliente</a></li>
      <li><a href="?/preguntasfrecuentes">Preguntas Frecuentes</a></li>
      <li><a href="?/encuesta">Encuestas</a></li>
    </ul>
  </li>

  <li><a href="tel:+018000910293"><i class="fa fa-mobile"></i>018000910293</a></li>
</ul>

        </div>
              </div>
    </nav>
  </header>
  <!-- // Main Container -->


    
<div class="jumbotron">
  
  <div class="carousel slide carousel-fade" data-ride="carousel" data-interval="3000">

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active"></div>
      <div class="item"></div>
      <div class="item"></div>
    </div>
  </div>


  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1>Compra tu boleto HOY</h1>
      </div>
      <div class="col-md-12">
      <div id="search_module">
   <form action="buscar.php" method="GET">
      <div class="row">
         <div class="col-xs-12 col-md-10">
            <div class="row">
               <div class="col-xs-12 col-sm-6 col-md-3">
                  <div class="v-autocomplete" tabindex="1">
                     <div class="v-autocomplete-input-group"><input id="txtOrigen" name="txtOrigen" type="search" placeholder="Ciudad de Origen" class="form-control control-map" onkeyup="mostrarOrigen(this.value)" onclick="this.value=''"></div>
                     <input type="hidden" id="txtOrigenOculto" value=0 name="id_origen"/>
                     <!---->
                     <div class="v-autocomplete-list" id='respuestaOrigen'>
                        
                    </div>
                  </div>
               </div>
               <div class="col-xs-12 col-sm-6 col-md-3">
                  <div class="v-autocomplete" tabindex="2">
                     <div class="v-autocomplete-input-group"><input id="txtDestino" name="txtDestino" type="search" placeholder="Ciudad de Destino" class="form-control control-map" onkeyup="mostrarDestino(document.getElementById('txtOrigenOculto').value,this.value)" onclick="this.value=''"></div>
                     <input type="hidden" id="txtDestinoOculto" name="id_destino" value=0/>
                     <!---->
                     <div class="v-autocomplete-list" id='respuestaDestino'>
                        
                    </div>
                  </div>
               </div>
               <div class="col-xs-6 col-md-3 col-sm-date">
                  <div class="vdp-datepicker" tabindex="3">
                     <div class="">
                        <!----> <input type="text" id="txtFechaSalida" name="salida" placeholder="Salida" required="required" readonly="readonly" class="form-control control-calendar"> <!---->
                     </div>
                  </div>
               </div>
               <div class="col-xs-6 col-md-3 col-sm-date">
                  <div class="vdp-datepicker" tabindex="4">
                     <div class="">
                        <!----> <input onClick="regreso(document.getElementById('txtFechaSalida').value)" type="text" id="txtFechaRegreso" name="regreso" placeholder="Regreso (opcional)" clear-button="true" readonly="readonly" class="form-control control-calendar"> <!---->
                     </div>
                     
                  </div>
               </div>
            </div>
         </div>
         <div class="col-xs-12 col-sm-6 col-md-2"><button analytics-on="click" analytics-event="busqueda-pinbus" analytics-category="Content Actions" id="btn_buscar" type="submit" tabindex="6" class="btn btn-primary btn-block gtm-buscar">Buscar</button></div>
      </div>
   </form>
</div>
</div>
    </div>
  </div>
</div>

<!-- Promociones -->
<section id="destinos" class="bg-white">
  <div class="container">
        <div class="row">
      <div class="col-md-12 text-center">
        <h1>Destinos mas buscados</h1>
        <h2>Encuentra las mejores ofertas, compra tu pasaje de bus y <strong>emprende tu viaje</strong></h2>
      </div>
    </div>
    
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 destino-promo">
        <div class="promotion" style="background-image: url(img/neiva-bogota.jpg)">
          <a href="?/buscar/6/15">
            <div class="promotion-content">
              <div class="highlight-content">
                <h5>Neiva a Bogota Salitre</h5>
                <div class="priceLinkSection">
                  <div class="separator"></div>
                  <div class="seoLinks">
                    <span class="price">
                      <small>Desde</small> $42.000
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </a>
        </div>
      </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 destino-promo">
        <div class="promotion" style="background-image: url(img/neiva-cali.jpg)">
          <a href="?/buscar/6/20">
            <div class="promotion-content">
              <div class="highlight-content">
                <h5>Neiva a Cali</h5>
                <div class="priceLinkSection">
                  <div class="separator"></div>
                  <div class="seoLinks">
                    <span class="price">
                      <small>Desde</small> $70.000
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </a>
        </div>
      </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 destino-promo">
        <div class="promotion" style="background-image: url(img/cali-ibague.jpg)">
          <a href="?/buscar/20/13">
            <div class="promotion-content">
              <div class="highlight-content">
                <h5>Cali a Ibague</h5>
                <div class="priceLinkSection">
                  <div class="separator"></div>
                  <div class="seoLinks">
                    <span class="price">
                      <small>Desde</small> $48.000
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </a>
        </div>
      </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 destino-promo">
        <div class="promotion" style="background-image: url(img/neiva-pitalito.jpg)">
          <a href="?/buscar/6/7">
            <div class="promotion-content">
              <div class="highlight-content">
                <h5>Neiva a Pitalito</h5>
                <div class="priceLinkSection">
                  <div class="separator"></div>
                  <div class="seoLinks">
                    <span class="price">
                      <small>Desde</small> $32.000
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </a>
        </div>
      </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 destino-promo">
        <div class="promotion" style="background-image: url(img/ibague-medellin.jpg)">
          <a href="?/buscar/13/1">
            <div class="promotion-content">
              <div class="highlight-content">
                <h5>Ibague a Medellin</h5>
                <div class="priceLinkSection">
                  <div class="separator"></div>
                  <div class="seoLinks">
                    <span class="price">
                      <small>Desde</small> $63.000
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </a>
        </div>
      </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 destino-promo">
        <div class="promotion" style="background-image: url(img/cali-florencia.jpg)">
          <a href="?/buscar/20/3">
            <div class="promotion-content">
              <div class="highlight-content">
                <h5>Cali a Florencia</h5>
                <div class="priceLinkSection">
                  <div class="separator"></div>
                  <div class="seoLinks">
                    <span class="price">
                      <small>Desde</small> $84.000
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </a>
        </div>
      </div>
          </div>
      </div>
</section>

<!-- Beneficios -->
<!-- benefits -->
<section id="benefits">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          <h3>Nuestros Beneficios</h3>
        </div>
      </div>
      <div class="row">
        <div class="benefit col-xs-12 col-sm-6 col-md-3">
          <img src="img/agencias-y-terminales.png?v=2ee410400a88b6b1a49baf268990351b" alt="Cobertura">
          <h4>Cobertura</h4>
          <p>Llegamos a 11 departamentos: Bogota DC, Antioquia, Quindio, Risaralda, Caldas, Cauca, Valle del Cauca, Tolima, Putumayo, Caqueta y Huila</p>
        </div>
        <div class="benefit col-xs-12 col-sm-6 col-md-3">
          <img src="img/noticias.png?v=f1b80432be63dc469aa5ecf8c2e4921a" alt="Encomiendas">
          <h4>Encomiendas</h4>
          <p>Ofrecemos el mejor servicio de envió y entrega de encomiendas.</p>
        </div>
        <div class="benefit col-xs-12 col-sm-6 col-md-3">
          <img src="img/servicios.png?v=244e8abc131a5d34797ac7df4c865cb8" alt="Estaciones de Servicio">
          <h4>Estaciones de Servicio</h4>
          <p>Visita nuestras estaciones de servicios en Neiva: EDS Aviatoor, EDS Norte, EDS Sur.</p>
        </div>
        <div class="benefit col-xs-12 col-sm-6 col-md-3">
          <img src="img/organizacion.png?v=c7a4a8fcbc7b3fb93bbcf1e04e834695" alt="Pasajes">
          <h4>Pasajes</h4>
          <p>Compra y adquieras con anticipación la ruta, la hora y la silla para que tengas un viaje tranquilo.</p>
        </div>
      </div>
    </div>
</section>

<!-- Rutas mas buscadas -->
<section id="most_visited_routes" class="bg-white">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <h4>Nuestras rutas más populares</h4>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-6 col-md-4">
          <ul>
                <li class="text-center">
                <a href="?/buscar/15/3?salida=2018-09-17">Bogota Salitre a Florencia</a>
                </li>
                <li class="text-center">
                    <a href="?/buscar/16/3?salida=2018-09-17">Bogota Sur a Florencia</a>
                </li>
                <li class="text-center">
                    <a href="?/buscar/6/1?salida=2018-09-17">Neiva a Medellin</a>
                </li>
                <li class="text-center">
                    <a href="?/buscar/20/3?salida=2018-09-17">Cali a Florencia</a>
                </li>
            </ul>
        </div>
        <div class="col-md-4"><ul><li class="text-center"><a href="?/buscar/17/3?salida=2018-09-17">Armenia a Florencia</a></li><li class="text-center"><a href="?/buscar/1/2?salida=2018-09-17">Medellin a La Dorada</a></li><li class="text-center"><a href="?/buscar/13/1?salida=2018-09-17">Ibague a Medellin</a></li><li class="text-center"><a href="?/buscar/18/3?salida=2018-09-17">Pereira a Florencia</a></li></ul></div><div class="col-md-4"><ul><li class="text-center"><a href="?/buscar/24/4?salida=2018-09-17">San Agustin a Garzon</a></li><li class="text-center"><a href="?/buscar/22/6?salida=2018-09-17">Popayan a Neiva</a></li></ul></div>    </div>
  </div>
</section>


      <footer id="footer">
  <div class="container">
    <div class="row">


      <div class="col-sm-5 pinbus_logo_social">
        <div class="social_links">
            <a target="_blank" href="#" class="facebook boxed-icon icon-2x" title=""><i class="fa fa-facebook"></i></a>
            <a target="_blank" href="#" class="twitter boxed-icon icon-1x" title=""><i class="fa fa-twitter"></i></a>
            <a target="_blank" href="#" class="google boxed-icon icon-1x" title=""><i class="fa fa-google-plus"></i></a>
            <a target="_blank" href="#" class="instagram boxed-icon icon-1x" title=""><i class="fa fa-instagram"></i></a>
            <div class="clearfix"></div>
          </div>
      </div>

      <div class="col-xs-6 col-xs-12 col-sm-2 ">
        <img src="img/tsconfig.png?v=97c18b87def6b14616f626082e603eb0" alt="Pinbus Redes Sociales" class="img-responsive center-block">
      </div>

      <div class="col-xs-6 col-xs-12 col-sm-3 pinbus_logo_social">
        <img src="img/AER.png?v=4ceea423a93b4ba350b6f6fc4ddd481a" alt="Pinbus Redes Sociales" class="img-responsive center-block">
      </div>
    </div>
  </div>
</footer>
  
  <!-- Scripts -->
  <!-- OPTIMIZE CSS DELIVERY -->
  <script>
  WebFontConfig={google:{families:['Roboto:300,400,700']}};(function(){var wf=document.createElement('script');wf.src=('https:'==document.location.protocol?'https':'http')+'://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';wf.type='text/javascript';wf.async='true';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(wf,s)})();
  </script>


 

  
 

  
  

  <script src="js/pinbus.min.js?v=5f780679a01d6e3609e4de04d4250472"></script>
  <!--FIN -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
   <script src="js/moment.min.js"></script>
   <script src="js/bootstrap-datetimepicker.min.js"></script>
   <script src="js/bootstrap-datetimepicker.es.js"></script>
   <script type="text/javascript">
   var hoy=new Date();
     $('#txtFechaSalida').datetimepicker({
          format: 'YYYY-MM-DD',
          pickTime: false
      });
      $('#txtFechaSalida').data("DatePicker").show();
   </script>
   <script type="text/javascript">
       $('#txtFechaRegreso').datetimepicker({
          format: 'YYYY-MM-DD',
          pickTime: false,
      });
      $('#txtFechaRegreso').data("DatePicker").show();
   </script>

  
</body>
</html>
