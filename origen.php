<?php
require_once 'conexion.php';//nos conectamos a la base de datos
$con=Conexion::conectar();//variable para conectarnos a la base de datos
$con->set_charset("utf8");//configuramos los caracteres de nuestra consulta
//recibir datos
//CODIFICACION PHP PARA MOSTRAR CIUDADES DE ORIGEN
//metodo isset que evalua si una variable existe o tiene algun valor
if(isset($_GET['origen']) && $_GET['origen']!=''){//si ya llego y no esta vacio
    $origen=$con->real_escape_string($_GET['origen']);//funcion de php que elimina caracteres extraños que lleguen a vulnerar la seguridad
    $resultado=$con->query("CALL buscarOrigen('$origen')");
    while($datos=$resultado->fetch_object()){//extraigo los datos que hay en resultado como si fuera un objeto
        ?>
        <div class="v-autocomplete-list-item" onclick="document.getElementById('txtOrigenOculto').value='<?php echo $datos->codigo_aeropuerto_origen; ?>';document.getElementById('txtOrigen').value='<?php echo '('.$datos->codigo.') '.$datos->nombre; ?>';mostrarOrigen('');document.getElementById('txtDestino').value='';document.getElementById('txtDestino').focus();">
            <div class="item-row"><span><b> <?php echo '('.$datos->codigo.') '.$datos->nombre; ?></b></span></div>
        </div>
        <?php
    }
}else if(isset($_GET['destino']) && $_GET['destino']!=''){
    $destino=$con->real_escape_string($_GET['destino']);
    $origen=$con->real_escape_string($_GET['id_origen']);
    $resultado=$con->query("CALL buscarDestino('$origen','$destino')");
    while($datos=$resultado->fetch_object()){
        ?>
        <div class="v-autocomplete-list-item" onclick="document.getElementById('txtDestinoOculto').value='<?php echo $datos->codigo_aeropuerto_destino; ?>';document.getElementById('txtDestino').value='<?php echo '('.$datos->codigo.') '.$datos->nombre; ?>';mostrarDestino('','');">
            <div class="item-row"><span><b> <?php echo '('.$datos->codigo.') '.$datos->nombre; ?></b></span></div>
        </div>
        <?php
    }
}
$con->close();
?>